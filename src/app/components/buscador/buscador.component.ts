import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService,Heroe } from '../../servicios/heroes.service';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html'
})
export class BuscadorComponent implements OnInit {

  heroes:any []=[];
  termino:string;

  constructor( private acticateRoutes:ActivatedRoute,
               private _heroesService:HeroesService  ) {

    }

  ngOnInit() {
    this.acticateRoutes.params.subscribe( params => {
      this.termino = params['termino'];
      this.heroes = this._heroesService.buscarHeroes( params ['termino']);
      console.log(this.heroes);
    });
  }

}
