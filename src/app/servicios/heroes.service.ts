import { Injectable } from '@angular/core';

@Injectable()

export class HeroesService {

  private heroes:Heroe[] =
[
  {
    nombre: "Spider-Man",
    bio: "Tras ser mordido por una araña se comnviente en la gaber",
    img:"assets/img/goku.png",
    aparicion:"En la Tierra 1962-08-01",
    casa:"Marvel"
  },
  {
    nombre: "Spider-Man",
    bio: "Tras ser mordido por una araña se comnviente en la gaber",
    img:"assets/img/images.jpg",
    aparicion:"En la Tierra 1962-08-01",
    casa:"Marvel"
  },
  {
    nombre: "Chocloman",
    bio: "Tras ser mordido por una Choclo se comnviente en la gaber",
    img:"assets/img/paper.png",
    aparicion:"En la Tierra 1962-08-01",
    casa:"Marvel"
  },
  {
    nombre: "Plinplinin",
    bio: "Tras ser mordido por una Plin plin se comnviente en la gaber, siendo el mejor del los plin plin",
    img:"assets/img/goku.png",
    aparicion:"1962-08-01",
    casa:"DC"
  },
];
  constructor() {

    //console.log("Servicio listo para consumir")
   }

   getHeroes():Heroe[]{
     return this.heroes;
   }

   getHeroe( idx: string ){
     return this.heroes[idx];
   }

   buscarHeroes( termino:string ) : Heroe[] {
     let heroesArr:Heroe[] = [];
     termino = termino.toLowerCase();

     for (let heroe of this.heroes){

       let nombre = heroe.nombre.toLowerCase();

       if( nombre.indexOf ( termino ) >=0 ){
          heroesArr.push ( heroe );
       }
     }

     return heroesArr;
   }
}

export interface Heroe{
  nombre: string;
  bio:  string;
  img: string;
  aparicion: string;
  casa: string;
};
